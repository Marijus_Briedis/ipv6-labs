#!/bin/bash

echo "Provisioning node01 now."
sudo su

echo "Beautiful bash prompt for easy typing."
cat /home/vagrant/.bashrc > /root/.bashrc

echo "Preparing the links and the network for routing."

ip link set down dev enp0s8

#ip link set up enp0s8
#ping the other node
