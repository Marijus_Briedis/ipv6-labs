#!/bin/bash

echo "Provisioning R1 now."
sudo su

echo "Beautiful bash prompt for easy typing."
cat /home/vagrant/.bashrc > /root/.bashrc

echo "Preparing the links and the network for routing."

ip link set down dev enp0s8
ip link set down dev enp0s9

cat >> /etc/network/interfaces << EOF

iface enp0s8 inet6 static
  address 2001:db8:98::1
  netmask 64
  autoconf 0
  dad-attempts 0
  accept_ra 0

iface enp0s9 inet6 static
  address 2001:db8:99::1
  netmask 64
  autoconf 0
  dad-attempts 0
  accept_ra 0
EOF

systemctl restart networking

ip link set up dev enp0s8
ip link set up dev enp0s9

echo 1 > /proc/sys/net/ipv4/ip_forward
echo 1 > /proc/sys/net/ipv6/conf/all/forwarding

echo "Preparing the radvd daemon."
apt install radvd
touch /etc/radvd.conf
cat >> /etc/radvd.conf << EOF
interface enp0s8
{
        AdvSendAdvert on;
        MinRtrAdvInterval 3;
        MaxRtrAdvInterval 10;
        AdvDefaultPreference low;
        AdvHomeAgentFlag off;
        prefix 2001:db8:98::/64
        {
                AdvOnLink on;
                AdvAutonomous on;
                AdvRouterAddr off;
        };
};

interface enp0s9
{
        AdvSendAdvert on;
        MinRtrAdvInterval 3;
        MaxRtrAdvInterval 10;
        AdvDefaultPreference low;
        AdvHomeAgentFlag off;
        prefix 2001:db8:99::/64
        {
                AdvOnLink on;
                AdvAutonomous on;
                AdvRouterAddr off;
        };
};
EOF
systemctl restart radvd
