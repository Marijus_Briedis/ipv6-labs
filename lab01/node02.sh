#!/bin/bash

echo "Provisioning node02 now."
sudo su

echo "Beautiful bash prompt for easy typing."
cat /home/vagrant/.bashrc > /root/.bashrc

echo "Preparing the links and the network for routing."

ip link set down dev enp0s8

#ip link set up enp0s8
#ip -6 addr add 2001:db8:98::cafe/64 dev enp0s8
#ip -6 route add 2001:db8:99::/64 via 2001:db8:98::1
#ping6 2001:db8:99::beef
