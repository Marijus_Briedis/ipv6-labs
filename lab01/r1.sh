#!/bin/bash

echo "Provisioning R1 now."
sudo su

echo "Beautiful bash prompt for easy typing."
cat /home/vagrant/.bashrc > /root/.bashrc

echo "Preparing the links and the network for routing."

ip link set down dev enp0s8
ip link set down dev enp0s9
echo 1 > /proc/sys/net/ipv4/ip_forward
echo 1 > /proc/sys/net/ipv6/conf/all/forwarding

ip link set up dev enp0s8
ip link set up dev enp0s9

ip -6 addr add 2001:db8:98::1/64 dev enp0s8
ip -6 addr add 2001:db8:99::1/64 dev enp0s9
