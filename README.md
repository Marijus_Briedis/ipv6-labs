### Vagrant commands
```
vagrant up
vagrant status
vagrant destroy -f
```

### IPv6 Lab01 static routing

Goals:  
- Show that the static route for IPv6 is the same  
- Configure the static route between 2 Debian/Ubuntu based hosts with ip command  
- Configure permanent interface file with IPv6  

Commands on node01:

```
ip link set up enp0s8
ip -6 addr add 2001:db8:99::beef/64 dev enp0s8
ip -6 route add 2001:db8:98::/64 via 2001:db8:99::1
ping6 2001:db8:98::cafe

vi /etc/network/interfaces

iface enp0s8 inet6 static
  address 2001:db8:99::beef
  netmask 64
  autoconf 0
  post-up ip -6 route add 2001:db8:98::/64 via 2001:db8:99::1

systemctl restart networking  
```
Commands on node02:

```
ip link set up enp0s8
ip -6 addr add 2001:db8:98::cafe/64 dev enp0s8
ip -6 route add 2001:db8:99::/64 via 2001:db8:98::1
ping6 2001:db8:99::beef

vi /etc/network/interfaces
iface enp0s8 inet6 static
  address 2001:db8:98::cafe
  netmask 64
  autoconf 0
  post-up ip -6 route add 2001:db8:99::/64 via 2001:db8:98::1

systemctl restart networking
```

### IPv6 Lab02 SLAAC configuration with radvd

Goals:  
- Show how SLAAC basic priciples  
- Show how NDP protocol works  

Commands on R1:
```
cd /vagrant/
tcpdump -n -i enp0s8 ip6 -w r1.pcap
```
On node01 and node02:
```
ip link set enp0s8 up
```
